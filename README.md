# Nexys 3 Utils

Useful HDL codes to utilize some of the devices available at the Nexys 3 development board.


<!-- ---------------------------- -->
## 16 Mbytes Cellular RAM - cellram_ctrl
The Nexys 3 development board has a 16 Mbytes Cellular RAM, which can operate at asynchronous, or synchronous mode. Here is found a simulation model (by **_Micron Technology_**) of the RAM, in Verilog, and a controller written in VHDL.


<!-- ---------------------------- -->
## 7-Segment Display - sevenseg_ctrl
A simple controller for the display found in the development board.
