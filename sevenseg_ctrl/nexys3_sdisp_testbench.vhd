-------------------------------------------------
-- Author:	Kevin P.M.
-- Date:	22 april, 2019
-- E-mail:
-------------------------------------------------
-- Title: 	Nexys 3 7-segment Display Controller
--			Testbench.
-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity nexys3_sdisp_testbench is
end entity nexys3_sdisp_testbench;

architecture behavior of nexys3_sdisp_testbench is

	-- seven segments display controller
	COMPONENT nexys3_sdisp is
		port
		(
			clk		:	in std_logic;
			en_l	:	in std_logic;
			test_l	:	in std_logic;
			din		:	in std_logic_vector(15 downto 0);
			dout	:	out std_logic_vector(7 downto 0);
			dsel	:	out std_logic_vector(3 downto 0)
		);
	end COMPONENT nexys3_sdisp;

	constant half_period	:	time := 5 ns;

	signal	clk		:	std_logic;
	signal	en_l	:	std_logic;
	signal	test_l	:	std_logic;

	signal	din		:	std_logic_vector(15 downto 0) := x"A2B3";
	signal	dout	:	std_logic_vector(7 downto 0);
	signal	dsel	:	std_logic_vector(3 downto 0);

	-- turns the output back to the input hexadecimal digit which is enabled,
	-- it turns easy to see the simulation
	signal	d_hex	:	std_logic_vector(3 downto 0);

	-- constants
	constant	none	:	std_logic_vector(7 downto 0) := (others => '1');
	constant	hex_0	:	std_logic_vector(6 downto 0) := "0000001";
	constant	hex_1	:	std_logic_vector(6 downto 0) := "1001111";
	constant	hex_2	:	std_logic_vector(6 downto 0) := "0010010";
	constant	hex_3	:	std_logic_vector(6 downto 0) := "0000110";
	constant	hex_4	:	std_logic_vector(6 downto 0) := "1001100";
	constant	hex_5	:	std_logic_vector(6 downto 0) := "0100100";
	constant	hex_6	:	std_logic_vector(6 downto 0) := "0100000";
	constant	hex_7	:	std_logic_vector(6 downto 0) := "0001111";
	constant	hex_8	:	std_logic_vector(6 downto 0) := "0000000";
	constant	hex_9	:	std_logic_vector(6 downto 0) := "0000100";
	constant	hex_A	:	std_logic_vector(6 downto 0) := "0000010";
	constant	hex_B	:	std_logic_vector(6 downto 0) := "1100000";
	constant	hex_C	:	std_logic_vector(6 downto 0) := "0110001";
	constant	hex_D	:	std_logic_vector(6 downto 0) := "1000010";
	constant	hex_E	:	std_logic_vector(6 downto 0) := "0010000";
	constant	hex_F	:	std_logic_vector(6 downto 0) := "0111000";

begin

	with dout(6 downto 0) select
		d_hex <=	x"0"	when	hex_0,
					x"1"	when	hex_1,
					x"2"	when	hex_2,
					x"3"	when	hex_3,
					x"4"	when	hex_4,
					x"5"	when	hex_5,
					x"6"	when	hex_6,
					x"7"	when	hex_7,
					x"8"	when	hex_8,
					x"9"	when	hex_9,
					x"A"	when	hex_A,
					x"B"	when	hex_B,
					x"C"	when	hex_C,
					x"D"	when	hex_D,
					x"E"	when	hex_E,
					x"F"	when	hex_F,
					"ZZZZ"	when	others;

	-----------------
	-- clock generation
	process
	begin

		clk	<= '0';

		loop

			wait for half_period;
			clk	<=	NOT(clk);

			wait for half_period;
			clk	<=	NOT(clk);


		end loop;
	end process;

	-----------------
	-- enable and test
	process
	begin

		en_l <= '0';

		loop

			wait for 40 ms;
			en_l <= NOT(en_l);
			
			wait for 20 ms;
			en_l <= NOT(en_l);

			wait for 960 ms;

		end loop;
	end process;

	process
	begin

		test_l <= '1';

		loop

			wait for 20 ms;
			test_l <= NOT(test_l);

			wait for 1000 ms;
			test_l <= NOT(test_l);

		end loop;
	end process;

	-----------------
	-- port map
	nexys3_sdisp_i: nexys3_sdisp
	port map
	(
		clk		=> clk,
		en_l	=> en_l,
		test_l	=> test_l,
		din		=> din,
		dout	=> dout,
		dsel	=> dsel
	);

end architecture behavior;
