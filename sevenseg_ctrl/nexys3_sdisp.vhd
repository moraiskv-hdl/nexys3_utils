-------------------------------------------------
-- Author:	Kevin P.M.
-- Date:	21 april, 2019
-- E-mail:
-------------------------------------------------
-- Title: 	Nexys 3 Seven Segment Display Controller
-- Description: decodes a 16 bits long input in to
-- a 4 digits hexadecimal value, and outputs each one
-- to a seven segment display available at the nexys
-- 3 development board. The entire display (4-digits)
-- refresh rate is of 2.5 ms. There is also a enable input,
-- and a test, which will start a 16 bits counter and
-- send its value to the output, when low.
-- The test counter is increased each second.
-- The decimal point is not used, and always disabled
-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nexys3_sdisp is
	port
	(
		clk		:	in std_logic;
		en_l	:	in std_logic;
		test_l	:	in std_logic;
		din		:	in std_logic_vector(15 downto 0);
		dout	:	out std_logic_vector(7 downto 0);
		dsel	:	out std_logic_vector(3 downto 0)
	);
end entity nexys3_sdisp;

architecture behavior of nexys3_sdisp is

	-- constants
	constant	none	:	std_logic_vector(7 downto 0) := (others => '1');
	constant	hex_0	:	std_logic_vector(6 downto 0) := "0000001";
	constant	hex_1	:	std_logic_vector(6 downto 0) := "1001111";
	constant	hex_2	:	std_logic_vector(6 downto 0) := "0010010";
	constant	hex_3	:	std_logic_vector(6 downto 0) := "0000110";
	constant	hex_4	:	std_logic_vector(6 downto 0) := "1001100";
	constant	hex_5	:	std_logic_vector(6 downto 0) := "0100100";
	constant	hex_6	:	std_logic_vector(6 downto 0) := "0100000";
	constant	hex_7	:	std_logic_vector(6 downto 0) := "0001111";
	constant	hex_8	:	std_logic_vector(6 downto 0) := "0000000";
	constant	hex_9	:	std_logic_vector(6 downto 0) := "0000100";
	constant	hex_A	:	std_logic_vector(6 downto 0) := "0000010";
	constant	hex_B	:	std_logic_vector(6 downto 0) := "1100000";
	constant	hex_C	:	std_logic_vector(6 downto 0) := "0110001";
	constant	hex_D	:	std_logic_vector(6 downto 0) := "1000010";
	constant	hex_E	:	std_logic_vector(6 downto 0) := "0010000";
	constant	hex_F	:	std_logic_vector(6 downto 0) := "0111000";

	-- input alias
	alias	d3_din	is	din(15 downto 12);
	alias	d2_din	is	din(11 downto 8);
	alias	d1_din	is	din(7 downto 4);
	alias	d0_din	is	din(3 downto 0);

	-- point segment (p), and each digit output of the decoder
	signal	p	:	std_logic := '1';
	signal	d3	:	std_logic_vector(6 downto 0);
	signal	d2	:	std_logic_vector(6 downto 0);
	signal	d1	:	std_logic_vector(6 downto 0);
	signal	d0	:	std_logic_vector(6 downto 0);

	-- digit select and digit to be decoded (can be de entity input, or test data)
	signal	sel		:	std_logic_vector(3 downto 0) := "0000";
	signal	d3_dec	:	std_logiC_vector(3 downto 0);
	signal	d2_dec	:	std_logiC_vector(3 downto 0);
	signal	d1_dec	:	std_logiC_vector(3 downto 0);
	signal	d0_dec	:	std_logiC_vector(3 downto 0);

	-- test
	signal	test_data	:	std_logic_vector(15 downto 0) := (others => '0');
	alias	test_d3	is	test_data(15 downto 12);
	alias	test_d2 is	test_data(11 downto 8);
	alias	test_d1	is	test_data(7 downto 4);
	alias	test_d0	is	test_data(3 downto 0);

begin

	---------------------
	-- output

	dsel <= NOT(sel)	when	(en_l = '0') else
			"1111";

	with sel select
		dout <=	p&d3	when	"1000",
				p&d2	when	"0100",
				p&d1	when	"0010",
				p&d0	when	"0001",
				none	when	others;

	---------------------
	-- timer, 10ms per digit
	process(clk)

		constant t_up	:	integer := 250000;	-- ~ 2.5ms with a 100 MHz clock
		variable count	:	integer range 0 to t_up := t_up;

	begin
		if (rising_edge(clk)) then
			if (en_l = '0') then
			-------------------------
				if (count = t_up) then
					count := 0;

					if (sel = "0000" OR sel = "1000") then
						sel <= "0001";

					elsif (sel = "0001") then
						sel <= "0010";
					
					elsif (sel = "0010") then
						sel <= "0100";

					elsif (sel = "0100") then
						sel <= "1000";
					else	-- prevents errors
						sel <= "0001";
					
					end if;

				else
					count := count + 1;

				end if;
			-------------------------
			else
				sel <= "0000";

			end if;
		end if;
	end process;

	---------------------
	-- test 7 segment controller

	d3_dec <=	d3_din	when (test_l = '1') else
				test_d3;
	d2_dec <=	d2_din	when (test_l = '1') else
				test_d2;
	d1_dec <=	d1_din	when (test_l = '1') else
				test_d1;
	d0_dec <=	d0_din	when (test_l = '1') else
				test_d0;

	-- process
	process(clk)

		constant	test_up		:	integer := 100000000;	-- ~ 1s with a 100 MHz clock
		-- constant	test_up		:	integer := 10000000;	-- ~100ms with a 100 MHz clock (useful for simulation)
		variable	test_count 	:	integer range 0 to test_up := 0;

	begin
		if (rising_edge(clk)) then
			if (en_l = '0' AND test_l = '0') then
			-----------------------------------
				if (test_count = test_up) then

					test_data <= std_logic_vector(unsigned(test_data) + 1);
					test_count := 0;

				else
					test_count := test_count + 1;

				end if;
			-----------------------------------
			end if;
		end if;
	end process;

	---------------------
	-- decoder
	with d0_dec select
		d0 <=	hex_0	when	x"0",
				hex_1	when	x"1",
				hex_2	when	x"2",
				hex_3	when	x"3",
				hex_4	when	x"4",
				hex_5	when	x"5",
				hex_6	when	x"6",
				hex_7	when	x"7",
				hex_8	when	x"8",
				hex_9	when	x"9",
				hex_A	when	x"A",
				hex_B	when	x"B",
				hex_C	when	x"C",
				hex_D	when	x"D",
				hex_E	when	x"E",
				hex_F	when	others;	-- x"F"

	with d1_dec select
		d1 <=	hex_0	when	x"0",
				hex_1	when	x"1",
				hex_2	when	x"2",
				hex_3	when	x"3",
				hex_4	when	x"4",
				hex_5	when	x"5",
				hex_6	when	x"6",
				hex_7	when	x"7",
				hex_8	when	x"8",
				hex_9	when	x"9",
				hex_A	when	x"A",
				hex_B	when	x"B",
				hex_C	when	x"C",
				hex_D	when	x"D",
				hex_E	when	x"E",
				hex_F	when	others;	-- x"F"

	with d2_dec select
		d2 <=	hex_0	when	x"0",
				hex_1	when	x"1",
				hex_2	when	x"2",
				hex_3	when	x"3",
				hex_4	when	x"4",
				hex_5	when	x"5",
				hex_6	when	x"6",
				hex_7	when	x"7",
				hex_8	when	x"8",
				hex_9	when	x"9",
				hex_A	when	x"A",
				hex_B	when	x"B",
				hex_C	when	x"C",
				hex_D	when	x"D",
				hex_E	when	x"E",
				hex_F	when	others;	-- x"F"

	with d3_dec select
		d3 <=	hex_0	when	x"0",
				hex_1	when	x"1",
				hex_2	when	x"2",
				hex_3	when	x"3",
				hex_4	when	x"4",
				hex_5	when	x"5",
				hex_6	when	x"6",
				hex_7	when	x"7",
				hex_8	when	x"8",
				hex_9	when	x"9",
				hex_A	when	x"A",
				hex_B	when	x"B",
				hex_C	when	x"C",
				hex_D	when	x"D",
				hex_E	when	x"E",
				hex_F	when	others;	-- x"F"

end architecture behavior;
