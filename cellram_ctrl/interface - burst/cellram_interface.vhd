--- Micron Cellular RAM controller, Burst Mode.
--- MT45W8MW16BGX
--- -sg708
--- clock of 50 MHz through a divisor of input 'clk'

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ram_pkg.all;

entity cellram_interface is
	port(
		clk			: in std_logic;		-- 100 MHz (available on nexys 3 development board)
		-- processor signals
		cancel_ram	: in std_logic;
		ready_req	: out std_logic;	-- ready flag, when '1' it says its done the memory operation
		ack_req		: out std_logic;	-- acknowledge flag, '1' means the ram controller has received the request
		request		: in std_logic;		-- request flag for a data read/write operation in the memory
		we_req		: in std_logic;		-- write flag, if '0' means a ready, on the other side, a write operation
		addr_bus	: in std_logic_vector(n_ramaddr-1 downto 0);
		data_bus	: inout std_logic_vector((n_ram*2)-1 downto 0);
		-- ram signals
		cre_r		: out std_logic;
		ce_r		: out std_logic;
		oe_r		: out std_logic;
		ub_r		: out std_logic;
		lb_r		: out std_logic;
		we_r		: out std_logic;
		clk_r		: out std_logic;
		adv_r		: out std_logic;
		wait_r		: in std_logic;
		addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
		data_ram	: inout std_logic_vector(n_ram-1 downto 0)
	);
end entity cellram_interface;

architecture behavior of cellram_interface is

	-----------------------
	-- FSM controller
	type state_machine	is	(INIT , CR1 , CR2 , CR3 , O , WB1 , WB2 , WB3 , WO , RB1 , RB2 , RB3 , RO);
	signal state	:	state_machine := INIT;

	-----------------------
	-- Control Register Address, only the bits [19:18] care
	constant BCR	:	std_logic_vector(19 downto 18) := "10";
	constant RCR	:	std_logic_vector(19 downto 18) := "00";
	constant DIDR	:	std_logic_vector(19 downto 18) := "01";

	-- BCR fields
	constant RES4	:	std_logic_vector(22 downto 20)	:= "000";	-- reserved (must be 0)
	constant RS		:	std_logic_vector(19 downto 18)	:= BCR;		-- register select
	constant RES3	:	std_logic_vector(17 downto 16)	:= "00";	-- reserved (must be 0)
	constant OM		:	std_logic_vector(15 downto 15)	:= "0";		-- operation mode ('1'=async mode; '0'=burst mode')
	constant IL		:	std_logic_vector(14 downto 14)	:= "0";		-- initial latency ('1'=fixed; '0'=variable)
	constant LC		:	std_logic_vector(13 downto 11)	:= "010";	-- latency counter ("010"=code 2; "011"=code 3)
	constant WP		:	std_logic_vector(10 downto 10)	:= "1";		-- wait polarity ('1'=Active HIGH; '0'=Active LOW)
	constant RES2	:	std_logic_vector(9 downto 9)	:= "0";		-- reserved (must be 0)
	constant WC		:	std_logic_vector(8 downto 8)	:= "1";		-- wait configuration ('0'=asserted during delay; '1'=asserted one data cycle before delay)
	constant RES	:	std_logic_vector(7 downto 6)	:= "00";	-- reserved (must be 0)
	constant DS		:	std_logic_vector(5 downto 4)	:= "01";	-- drive strength ("01"=1/2; "00"=full; "10"=1/4; "11"=reserved)
	constant BW		:	std_logic_vector(3 downto 3) 	:= "1";		-- burst wrap ('1'=disabled; '0'=enabled)
	constant BL		:	std_logic_vector(2 downto 0) 	:= "001";	-- burst length ("001"=4Words; "010"=8Words; "011"=16Words; "111"=Continuous)

	-----------------------
	-- Burst Mode N Words
	-- Number of 32 bits words reads in one request, defined by constant BL (burst length)
	signal  burst_n	:	integer range 0 to 16;

	-----------------------
	-- Constants to count time, using the clk_ram (even during the async initial mode) of 50 MHz (T = 20 ns)
	constant init_num : integer	:= 8500;

	constant cr1_c	: integer := 0;
	constant cr2_c	: integer := cr1_c + 3;

	-- count signals
	signal init_count	:	integer range 0 to init_num := 0;	-- power up initial counter
	signal word_count	:	integer range 0 to 15;				-- count how many words have been read, or CR access time (in 50MHz clock cycles)
	signal new_word		:	std_logic := '0';
	signal half_sel		:	std_logic := '0';	-- flag to select which half of the word (31:16 or 15:0) will be read/written

	-- register to store the operation address, data read from ram, and write to it
	signal addr_reg		:	std_logic_vector(n_ramaddr-1 downto 0) := (others => '0');	-- register to save the addr access
	signal data_read	:	std_logic_vector((n_ram*2)-1 downto 0) := (others => '0');	-- register to save data read from the RAM

	-- temp signals
	signal	ready_ram_en	:	std_logic	:= '1';
	signal	ready_ram_t		:	std_logic	:= '0';
	signal	adv_t			:	std_logic	:= '1';
	signal	ce_t			:	std_logic	:= '1';
	signal	we_t			:	std_logic	:= '1';
	signal	ulb_r_t			:	std_logic	:= '1';
	signal	ulb_r			:	std_logic;

	-- ram clock
	signal	clk_ram		:	std_logic := '0';

	-- acknowledge flag
	signal	ack_t		:	std_logic := '0';
	signal	ack_en		:	std_logic := '0';

	-- address signals to keep track of the read/write operation in case of reaching the 128 words row end
	signal	addr_next_row	:	std_logic_vector(n_ramaddr-1 downto 0);
	signal	addr_upper_sum	:	std_logic_vector(n_ramaddr-1 downto 7);

begin

	-----------------------------------------------------------------------		
	-----------------------------------------------------------------------		
	-----------------------------------------------------------------------		
	-- Attached Device Interface

	----------------------------------
	-- Sync the ready_req with the attached device clock, so if there is a fast device (like a core)
	-- it doesnt see multiples cycles of < ready_req = '1' >
	process(clk)
	begin
		if (rising_edge(clk)) then
			
			if (ready_ram_t = '1' AND ready_ram_en = '1') then
				ready_ram_en <= '0';
			elsif (ready_ram_t = '0') then	-- wait for ready_ram_t to go low before re-enable the logic
				ready_ram_en <= '1';
			end if;

		end if;
	end process;

	ready_req	<=	ready_ram_t	when	(ready_ram_en = '1')	else
					'0';

	ready_ram_t	<=	'1'	when	(new_word = '1') else
					'0';

	----------------------------------
	-- as with the 'ready_req' signal, the 'ack' also is sync with the input system clk
	ack_req <=	ack_t when	(ack_en = '1') else
				'0';

	process(clk)
	begin
		if (rising_edge(clk)) then

			if (ack_t = '1' AND ack_en = '1') then
				ack_en <= '0';
			elsif (ack_t = '0') then
				ack_en <= '1';
			end if;
		
		end if;
	end process;

	process(clk_ram)
	begin
		if (rising_edge(clk_ram)) then
			CASE state is
				--------------------------
				when O =>
					if (request = '1') then
						ack_t <= '1';
					end if;
				--------------------------
				when others =>
					ack_t <= '0';
				--------------------------
			end CASE;
		end if;
	end process;

	-----------------------------------------------------------------------
	-----------------------------------------------------------------------
	-----------------------------------------------------------------------
	-- RAM Interface

	-- RAM Burst Mode, Read Length
	with BL	select
		burst_n <=	4	when	"001",
					8	when	"010",
					16	when	"011",
					0	when	others;

	-- RAM CLK_R, clock
		-- clk_r must be held LOW or HIGH during the CR configuration
		-- states, since it start defaulting to async mode
	process(clk)
	begin
		if (rising_edge(clk)) then
			clk_ram <= NOT(clk_ram);
		end if;
	end process;

	with state select
		clk_r <=	'0'			when	INIT | CR1 | CR2 | CR3,
					clk_ram		when	others;

	----------------------------------
	-- 'advr_r' must be LOW for the CR configuration, since the memory power up defaulting to async mode
	-- 'adv_r' must be LOW for the first cycle of read/write op in burst mode
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------
				when RB1 | WB1 =>
					adv_t <= '0';
				-------------------------
				when others =>
					adv_t <= '1';
				-------------------------
			end CASE;
		end if;
	end process;

	with state select
		adv_r <=	'1'		when	INIT,
					'0'		when	CR1 | CR2 | CR3,
					adv_t	when	others;

	-- RAM CE, chip enable
	-- obs1: ce_n needs a pulse every tCEM=4us
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------
				when RB1 | WB1 =>
					ce_t <= '0';
				-------------------------
				when WB3 =>
					if (we_req = '0') then
						ce_t <= '1';
					end if;
				-------------------------
				when RB3 =>
					if (cancel_ram = '1' OR word_count = burst_n) then
						ce_t <= '1';
					end if;
				-------------------------
				when O | WO | RO =>
						ce_t <= '1';
				-------------------------
				when others =>
					null;
			end CASE;
		end if;
	end process;

	with state select
		ce_r 	<=	'1'		when	INIT,
					'0'		when	CR1 | CR2,
					ce_t	when	others;

	-- RAM WE, write enable
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------
				when WB1 =>
					we_t <= '0';
				-------------------------
				when others =>
					we_t <= '1';
				-------------------------
			end CASE;
		end if;
	end process;

	with state select
		we_r 	<=	'1'		when	INIT | CR1 | CR3,
					'0'		when	CR2,
					we_t	when	others;

	-- RAM CRE, control register enable
	with state select
		cre_r<=	'1' when	CR1 | CR2,
				'0' when	others;

	-- RAM UP and LB, upper and lower byte enable
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------
				when RB1 | WB1 =>
					ulb_r_t <= '0';
				-------------------------
				when O | WO | RO =>
					ulb_r_t <= '1';
				-------------------------
				when others =>
					null;
				-------------------------
			end CASE;
		end if;
	end process;

	with state select
		ulb_r	<=	'1'		when	INIT | CR3,
					'0'		when	CR1 | CR2,
					ulb_r_t	when	others;

	ub_r <= ulb_r;
	lb_r <= ulb_r;	

	-- RAM OE, output enable
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------
				when RB2 | RB3 =>
					oe_r <=	'0';
				-------------------------
				when others =>
					oe_r <= '1';
				-------------------------
			end CASE;
		end if;
	end process;

	------------------------------------------
	-- RAM Address
	process(clk_ram)
	begin
		if (rising_edge(clk_ram)) then
			CASE state is
				---------------------------
				when INIT =>
					addr_reg <= RES4&RS&RES3&OM&IL&LC&WP&RES2&WC&RES&DS&BW&BL;	-- during the CR configuration, the address hold the data to be saved
				---------------------------
				when O =>
					if (request = '1') then
						addr_reg <= addr_bus;
					end if;
				---------------------------
				when WO | RO =>
					addr_reg <= addr_next_row;
				---------------------------
				when others =>
					null;
				---------------------------
			end CASE;
		end if;
	end process;

	addr_ram <=	addr_reg;

	-- next row address
	addr_upper_sum	<=	std_logic_vector(unsigned(addr_reg(n_ramaddr-1 downto 7)) + 1);
	addr_next_row	<= addr_upper_sum & b"000_0000";

	------------------------------------------
	-- RAM Read Data
	process(clk_ram)
	begin
		if (rising_edge(clk_ram)) then
			CASE state is
				--------------------------
				when RB3 =>
					if (word_count < burst_n) then
						if (half_sel = '1') then
							data_read(31 downto 16) <= data_ram;
						else
							data_read(15 downto 0) <= data_ram;
						end if;
					end if;
				--------------------------
				when others =>
					null;
				--------------------------
			end CASE;
		end if;
	end process;

	with state select
		data_bus <=	data_read		when	RB3 | RO,
					(others => 'Z')	when	others;

	------------------------------------------
	-- RAM Write Data
	data_ram <= data_bus(31 downto 16)	when	(half_sel = '1' AND (state = WB3)) else
				data_bus(15 downto 0)	when	(half_sel = '0' AND (state = WB3)) else
				(others => 'Z');

	------------------------------------------
	-- Counters

	-- Power Up Counter Process:
	-- DRAM start up time, datasheet says it takes 150us to power up the device,
	-- its used a 50 MHz clock signal here to count time
	process(clk_ram)
	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------------------------
				when INIT =>
					init_count <= init_count + 1;
				-------------------------------------------
				when others =>
					null;
				-------------------------------------------
			end CASE;
		end if;
	end process;

	-- Word Count Process
	process (clk_ram)
	begin
		if (rising_edge(clk_ram)) then
			CASE state is
				-------------------------------------------
				when CR1 | CR2 =>
					word_count <= word_count + 1;
				-------------------------------------------
				when WB3 =>
					if (half_sel = '1' AND wait_r = '0') then	-- on write op the wait is asserted during the last valid cycle, so we need to check wait_r
						word_count	<= word_count + 1;
						new_word	<= '1';
					else
						new_word	<= '0';
					end if;
				-------------------------------------------
				when RB3 =>
					if (half_sel = '1') then	-- wait is asserted one cycle before invalid data, so there is no need to check, since the state will change
						word_count	<= word_count + 1;
						new_word	<= '1';
					else
						new_word	<= '0';
					end if;
				-------------------------------------------
				when INIT | O =>
					word_count <= 0;
				-------------------------------------------
				when others =>
					null;
				-------------------------------------------
			end CASE;
		end if;
	end process;

	-- Half Word Count Process
	process(clk_ram)

		variable burst_ready : std_logic := '0';

	begin
		if (falling_edge(clk_ram)) then
			CASE state is
				-------------------------------------------
				when WB3 | RB3 | RO =>
	
					if (burst_ready = '1') then
						half_sel <= NOT(half_sel);
					end if;

					burst_ready := '1';
				-------------------------------------------
				when others =>
					burst_ready := '0';
				-------------------------------------------
			end CASE;
		end if;
	end process;

	------------------------------------------
	-- FSM process
	process(clk_ram)
	begin
		if (rising_edge(clk_ram)) then
			CASE state is
				------------------------------
				when INIT =>
					if (init_count = init_num) then
							state <= CR1;
					end if;
				------------------------------
				when CR1 =>
					if (word_count = cr1_c) then
						state <= CR2;
					end if;
				
				when CR2 =>
					if (word_count = cr2_c) then
						state <= CR3;
					end if;

				when CR3 =>
					state <= O;
				------------------------------
				when O =>
					if (request = '1') then
						if (we_req = '1') then
							state <= WB1;

						else
							state <= RB1;
						end if;
					end if;
				------------------------------
				when WB1 =>
					state <= WB2;

				when WB2 =>						-- ram access cannot be cancelled during latency delay, as it is possible to have data corruption
					if (wait_r = '0') then
						state 		<= WB3;
					end if;

				when WB3 =>
					if (we_req = '0') then		-- write burst doesnt have n word limit, keep going until the 128 words row ends, which will
						state	<= O;			-- then restart the write cycle, as long as we_req keep active (LOW)
					elsif (wait_r = '1') then
						state	<= WO;
					end if;

				when WO =>
					state <= WB1;
				------------------------------
				when RB1 =>
					state <= RB2;

				when RB2 =>						-- ram access cannot be cancelled during latency delay, as it is possible to have data corruption
					if (wait_r = '0') then
						state <= RB3;
					end if;

				when RB3 =>
					if (cancel_ram = '1' OR word_count = burst_n) then	-- stop ta burst length or a cancel flag from the attached device,
						state <= O;										-- else it keep reading on the next 128 words row
					elsif (wait_r = '1') then
						state <= RO;
					end if;

				when RO =>
					state <= RB1;
				------------------------------
			end CASE;
		end if;
	end process;


end architecture behavior;
