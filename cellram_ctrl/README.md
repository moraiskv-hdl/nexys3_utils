# Cellular RAM Controller

The main VHDL files are:

- cellram_interface.vhd
- cellram_pkg.vhd
- cellram_interface.ucf

As there is two **_cellram_interface.vhd_** files (burst and async), the folder name says the operation mode (async / burst).

**NOTE: at the moment, the burst mode isn't working.**


<!-- ----------------------- -->
## Testbench

Tests can be done with the files inside **_testbench_** folder, and with a Verilog model of the RAM, provided by **MICRON** (folder **_cellram_micron_model-VERILOG_**).


<!-- ----------------------- -->
## TODO

- better documentation
- finish burst mode
