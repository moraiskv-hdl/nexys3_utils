--- Micron Cellular RAM controller, Page Mode.
--- MT45W8MW16BGX
--- -sg708
--- clock of 100 MHz

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cellram_pkg.all;

entity cellram_interface is
	port(
		clk			: in std_logic;		-- 100 MHz (available on nexys 3 development board)
		-- processor signals
		stop_req	: in std_logic;
		we_req		: in std_logic;		-- write flag, if '0' means a ready, on the other side, a write operation
		ready_req	: out std_logic;	-- ready flag, when '1' it says its done the memory operation
		ack_req		: out std_logic;
		request		: in std_logic;		-- request flag for a data read/write operation in the memory
		addr_bus	: in std_logic_vector(n_ramaddr-1 downto 0);
		data_bus	: inout std_logic_vector((n_ram*2)-1 downto 0);
		-- ram signals
		cre_r		: out std_logic;
		ce_r		: out std_logic;
		oe_r		: out std_logic;
		ub_r		: out std_logic;
		lb_r		: out std_logic;
		we_r		: out std_logic;
		clk_r		: out std_logic;
		adv_r		: out std_logic;
		wait_r		: in std_logic;
		addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
		data_ram	: inout std_logic_vector(n_ram-1 downto 0)
	);
end entity cellram_interface;

architecture behavior of cellram_interface is

	-----------------------
	-- FSM controller
	type state_machine	is	(INIT , CR1 , CR2 , CR3 , O , W1 , W2 , W3 , R1 , R2 , R3 , R4 ,P);
	signal	state	:	state_machine := INIT;

	-----------------------
	-- Control Register Address, only the bits [19:18] care
	constant BCR	:	std_logic_vector(19 downto 18) := "10";
	constant RCR	:	std_logic_vector(19 downto 18) := "00";
	constant DIDR	:	std_logic_vector(19 downto 18) := "01";

	-- RCR fields
	constant RES4	:	std_logic_vector(22 downto 20)	:= "000";		-- reserved (must be 0)
	constant RS		:	std_logic_vector(19 downto 18)	:= RCR;			-- register select
	constant RES3	:	std_logic_vector(17 downto 8)	:= b"00_0000_0000";	-- reserved (must be 0)
	constant PAGE	:	std_logic_vector(7 downto 7)	:= "1";			-- page mode ('1'=enabled; '0'=disabled)
	constant RES2	:	std_logic_vector(6 downto 5) 	:= "00";		-- reserved (must be 0)
	constant DPD	:	std_logic_vector(4 downto 4)	:= "1";			-- deep power-down ('1'=disabled; '0'=enabled)
	constant RES	:	std_logic_vector(3 downto 3)	:= "0";			-- reserved (must be 0)
	constant PAR	:	std_logic_vector(2 downto 0) 	:= "000";		-- array refresh ("000"=full_array) 

	-----------------------
	-- Page Mode Enable (default enabled)
	signal	page_en		:	std_logic	:= '1';	-- when '0' it will ignore the initial CR states, and work at default async mode
	-- signal	page_en		:	std_logic	:=	'0';
	signal	page_end	:	std_logic;			-- when '1' it means end of a 16 word page

	-----------------------
	-- Constants to count access time
	constant init_c	: integer := 17000;	-- 170 us with a 100 MHz clock
	constant t1_c	: integer := 2;
	constant t2_c	: integer := t1_c + 6;
	constant p_c	: integer := 2;

	-- count signals
	signal	ce_count	:	integer range 0 to init_c	:= 0;	-- power up initial counter to enable the device
	signal	count		:	integer range 0 to t2_c 	:= 1;	-- count how many cycles it needs to satisfies the IC time requirements

	signal	word_count	:	std_logic := '0';	-- it says which part of the 32 bits word is being read/write, '1'=[31:16] and '0'=[15:0]
	signal	last_word	:	std_logic := '0';	-- when '1', it means that after going from state R4 to R1 because of a 'page_end', then
												-- the access must finish after the first 16 bits words read, so we avoid infinite loops
												-- going from state R4 to R1 due to incomplete 32 bits words at page end

	-- register to store the operation address, data read from ram, and write to it
	signal	addr_next		:	std_logic_vector(n_ramaddr-1 downto 0);						-- addr_reg + 1 (next 16 bits words, so we can form a 32 bits data)
	signal	addr_reg		:	std_logic_vector(n_ramaddr-1 downto 0) := (others => '0');	-- register to save the addr access
	signal	data_read		:	std_logic_vector((n_ram*2)-1 downto 0) := (others => '0');	-- register to save data read from the RAM
	signal	data_write_H	:	std_logic_vector(n_ram-1 downto 0) := (others => '0');		-- register to save data write from the cache to the RAM, saves the upper part
	signal	data_write_L	:	std_logic_vector(n_ram-1 downto 0) := (others => '0');		-- saves the lower parte, it's the one saved in to the pipeline output write register
	signal	half_data_write	:	std_logic_vector(n_ram-1 downto 0);							-- pipeline output register

	-- temp signals
	signal	ub_r_t	:	std_logic;	-- temp signal for ub_r and lb_r (they are always active together in this controller design)

	-- ready signals
	signal	ready_page	:	std_logic;
	signal	ready_read	:	std_logic;
	signal	ready_write	:	std_logic;

	-- stop signals, when '1', it stop the read op, and get the FSM back to state O
	signal	stop_read	:	std_logic;
	signal	stop_page	:	std_logic;

begin

	----------------------------------
	-- Acknowledge output
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE STATE is
				----------------------------
				when O =>
					if (request = '1') then
						ack_req <= '1';
					end if;
				----------------------------
				when others =>
					ack_req <= '0';
				----------------------------
			end CASE;
		end if;
	end process;

	----------------------------------
	-- Hold static low for the entire read/write async operation
	adv_r	<= '0';
	clk_r	<= '0';

	----------------------------------
	-- Outputs

	-- Ready output flag
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------------
				when R4 =>
					if (word_count = '1' AND page_en = '1') then
						ready_page <= '1';	-- ready flag will be '1' for one cycle, in the state P
					else
						ready_page <= '0';
					end if;
				----------------------------------
				when others =>
					ready_page <= '0';
				----------------------------------
			end CASE;
		end if;
	end process;

	ready_read	<=	'1'	when (state = R4 AND word_count = '1' AND page_en = '0')	else
					'0';

	ready_write	<=	'1'	when (state = W3 AND word_count = '1')	else
					'0';

	ready_req	<=	ready_page OR ready_read OR ready_write;

	-- RAM inputs
	with state select
		cre_r<=	'1' when	CR1 | CR2,
				'0' when	others;

	with state select
		ce_r <=	'1'	when	INIT | CR3 | O | W3,
				'0'	when	others;

	-- upper and lower byte are always high and low together (no byte select logic used)
	with state select
		ub_r_t	<=	'0'	when	R1 | R2 | R3 | R4 | W1 | W2 | P,
					'1'	when	others;

	ub_r <= ub_r_t;
	lb_r <= ub_r_t;
	
			
	-- Read Op
	with state select
		oe_r <=	'0' when	R2 | R3 | R4 | P,
				'1'	when	others;

	-- Write op
	with state select
		we_r <=	'0' when	W2 | CR2,
				'1'	when	others;

	------------------------------------------
	-- RAM Address
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				------------------------------------
				when INIT =>
					if (page_en = '1') then
						addr_reg <= RES4&RS&RES3&PAGE&RES2&DPD&RES&PAR;
					else
						addr_reg	<= (others => '0');
					end if;
				------------------------------------
				when O =>
					if (request = '1') then
						addr_reg <= addr_bus;
					end if;
				------------------------------------
				when W3 | R4 =>
					addr_reg <=	addr_next;
				------------------------------------
				when others =>
					null;
				------------------------------------
			end CASE;
		end if;
	end process;

	addr_next<=	std_logic_vector(unsigned(addr_reg) + 1);

	addr_ram <= addr_reg;

	-- verify if the page access is outside the 16 words page, if TRUE, the flag will cause the fsm to go back to state R1, and start a new access
	-- no need to verify the state, since this signal is only check in P OR R4 state
	page_end <= '1'	when (addr_reg(3 downto 0) = "1111" AND page_en = '1') else
				'0';

	------------------------------------------
	-- Read Data
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------------
				when R3 =>
					if (page_en = '0') then
						if (word_count = '0') then
							data_read(15 downto 0) <= data_ram;
						else
							data_read(31 downto 16) <= data_ram;
						end if;
					end if;
				----------------------------------
				when R4 =>
				if (page_en = '1') then
					if (word_count = '0') then
						data_read(15 downto 0) <= data_ram;
					else
						data_read(31 downto 16) <= data_ram;
					end if;
				end if;
				when others =>
					null;
				----------------------------------					
			end CASE;
		end if;
	end process;

	data_bus <= data_read	when	(ready_page = '1' OR ready_read = '1') else
				(others => 'Z');

	------------------------------------------
	-- Write Data
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------------
				when O =>
					if (request = '1' AND we_req = '1') then
						data_write_H <= data_bus((n_ram*2)-1 downto n_ram);
						data_write_L <=	data_bus(n_ram-1 downto 0);
					end if;
				----------------------------------
				when W3 =>
					data_write_L	<=	data_write_H;
				----------------------------------
				when others =>
					null;
				----------------------------------
			end CASE;
		end if;
	end process;

	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------------
				when W1 =>
					half_data_write <= data_write_L;
				----------------------------------
				when others =>
					null;
				----------------------------------
			end CASE;
		end if;
	end process;

	with state select
		data_ram <= half_data_write	when	W2 | W3,
					(others => 'Z')	when	others;

	------------------------------------------
	-- Counters

	-- Power Up Counter Process:
	-- DRAM start up time, datasheet says it takes 150us to power up the device,
	-- its used a 100 MHz clock signal here to count time;
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------
				when INIT =>
					ce_count <= ce_count + 1;
				----------------------------
				when others =>
					null;
				----------------------------
			end CASE;
		end if;
	end process;

	-- Count Process
	process(clk)
	begin
		if (rising_edge(clk)) then

			CASE state is
				-------------------------------------------
				when CR1 | CR2 | R1 | R2 | W1 | W2 | P =>
					count <= count + 1;
				-------------------------------------------
				when others =>
					count <= 1;
				-------------------------------------------
			end CASE;

		end if;
	end process;

	-- Half Word Count Process
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				-----------------------------------
				when W3 | R4 =>
					word_count <= NOT(word_count);
				-----------------------------------
				when O =>
					word_count <= '0';
				-----------------------------------
				when others =>
					null;
				-----------------------------------
			end CASE;
		end if;
	end process;

	-- Last Word Count Process
	process(clk)
		variable hold	:	std_logic := '0';
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------------
				when R4 =>
					if (stop_req = '0' AND hold = '0') then
						if (page_end = '1' AND word_count = '0') then
							last_word <= '1';
							hold := '1';
						end if;
					elsif (hold = '1') then
						last_word <= '0';
						hold := '0';
					end if;
				----------------------------------
				when O =>
					last_word <= '0';
				----------------------------------
				when others =>
					null;
				----------------------------------
			end CASE;
		end if;
	end process;

	------------------------------------------
	-- FSM
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				------------------------------
				when INIT =>
					if (ce_count = init_c) then
						if (page_en = '1') then
							state <= CR1;
						else
							state <= O;
						end if;
					end if;
				------------------------------
				when CR1 =>
					if (count = t1_c) then
						state <= CR2;
					end if;
				
				when CR2 =>
					if (count = t2_c) then
						state <= CR3;
					end if;

				when CR3 =>
					state <= O;
				------------------------------
				when O =>
					if (request = '1' AND stop_req = '0') then
						if (we_req = '1') then
							state <= W1;

						else
							state <= R1;
						end if;
					end if;
				------------------------------
				when W1 =>
					if (count = t1_c) then
						state <= W2;
					end if;

				when W2 =>
					if (count = t2_c) then
						state <= W3;
					end if;

				when W3 =>
					if (word_count = '1') then
						state <= O;
					else
						state <= W1;
					end if;
				------------------------------
				when R1 =>
					if (stop_req = '1') then
						state <= O;
					elsif (count = t1_c) then
						state <= R2;
					end if;

				when R2 =>
					if (stop_req = '1') then
						state <= O;
					elsif (count = t2_c) then
						state <= R3;
					end if;

				when R3 =>
					if (stop_req = '1') then
						state <= O;
					else
						state <= R4;
					end if;

				when R4 =>
					if (stop_req = '1' OR stop_read = '1') then
						state <= O;
					elsif (page_en = '0' OR (page_end = '1' AND word_count = '0')) then	-- if page end is reached, and only half of a 32b word is done loading,
						state <= R1;													-- then its done one more read, through R1, so we can finish this word
					else
						state <= P;
					end if;
				------------------------------
				when P =>
					if (stop_req = '1' OR stop_page = '1') then
						state 	<= O;
					elsif (count = p_c) then
						state <= R4;
					end if;
				------------------------------
			end CASE;
		end if;
	end process;

	-- flag to stop async default read
	stop_read <= '1' when	(page_en = '0' AND word_count = '1')	else
				 '0';	

	-- flag to stop page read
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				------------------------
				when R4 =>
					if (last_word = '1' OR (page_end = '1' AND word_count = '1')) then
						stop_page <= '1';
					end if;
				------------------------
				when others =>
					stop_page <= '0';
				------------------------
			end CASE;
		end if;
	end process;

end architecture behavior;
