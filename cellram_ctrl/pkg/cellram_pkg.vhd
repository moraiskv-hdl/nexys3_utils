library ieee;
use ieee.std_logic_1164.all;

package cellram_pkg is

	constant n_ram		:	integer := 16;
	constant n_ramaddr	:	integer := 23;

	-- RAM Interface
	COMPONENT cellram_interface is
		port(
			clk			: in std_logic;
			-------------------------
			stop_req	: in std_logic;
			ready_req	: out std_logic;
			ack_req		: out std_logic;
			request		: in std_logic;
			we_req		: in std_logic;
			addr_bus	: in std_logic_vector(n_ramaddr-1 downto 0);
			data_bus	: inout std_logic_vector((n_ram*2)-1 downto 0);
			-------------------------
			cre_r		: out std_logic;
			ce_r		: out std_logic;
			oe_r		: out std_logic;
			ub_r		: out std_logic;
			lb_r		: out std_logic;
			we_r		: out std_logic;
			clk_r		: out std_logic;
			adv_r		: out std_logic;
			wait_r		: in std_logic;
			addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
			data_ram	: inout std_logic_vector(n_ram-1 downto 0)
		);
	end COMPONENT cellram_interface;


end package cellram_pkg;
