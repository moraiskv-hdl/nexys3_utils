library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cellram_tb_pkg.all;

entity cellram_tb is
	port
	(
		clk			: in std_logic;
		rst_bt		: in std_logic;
		rst_led		: out std_logic;
		--------------------------
		sel_addr_bt	: in std_logic;
		sel_rst_bt	: in std_logic;
		sel_up_bt	: in std_logic;
		sel_reg_bt	: in std_logic_vector(1 downto 0);
		dsel		: out std_logic_vector(3 downto 0);
		dout_sdisp	: out std_logic_vector(7 downto 0);
		sel_sdisp_bt: in std_logic;
		--------------------------
		clk_r		: out std_logic;
		adv_r		: out std_logic;
		cre_r		: out std_logic;
		wait_r		: in std_logic;
		ce_r		: out std_logic;
		oe_r		: out std_logic;
		we_r		: out std_logic;
		lb_r		: out std_logic;
		ub_r		: out std_logic;
		addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
		data_ram	: inout std_logic_vector(n_ram-1 downto 0)
	);
end entity cellram_tb;

architecture behavior of cellram_tb is

	-- seven seg display
	signal	en_l_sdisp	:	std_logic;
	signal	test_l_sdisp:	std_logic;
	signal	din_sdisp	:	std_logic_vector(15 downto 0);
	signal	dout_sdisp_t:	std_logic_vector(7 downto 0);

	-- reset
	signal	rst		:	std_logic;
	signal	rst_int	:	std_logic := '1';
	
	-- buttons
	signal	sel_reg		:	std_logic_vector(1 downto 0);
	signal	sel_up		:	std_logic;
	signal	sel_sdisp	:	std_logic;
	signal	sel_rst		:	std_logic;
	signal	sel_addr	:	std_logic;
	signal	rst_btd		:	std_logic;

	-- ram interface
	signal	stop_req	:	std_logic;
	signal	ready_req	:	std_logic;
	signal	ack_req		:	std_logic;
	signal	request		:	std_logic;
	signal	request_t	:	std_logic;
	signal	we_req		:	std_logic;
	signal	we_ram_t	:	std_logic;
	signal	addr_bus	:	std_logic_vector(n_ramaddr-1 downto 0) := (others => '0');
	signal	data_bus	:	std_logic_vector((n_ram*2)-1 downto 0);
	
	-- reg file
	constant n_regfile	:	integer := 4;
	type mem_array is ARRAY (0 to n_regfile-1) of std_logic_vector((n_ram*2)-1 downto 0);
	signal	regfile		:	mem_array := (
											x"1234_5678",
											x"9abc_def0",
											x"5678_1234",
											x"def0_9abc");

	signal	re_ram		:	std_logic;
	signal	we_regfile	:	std_logic;
	signal	pointer		:	integer range 0 to n_regfile-1;
	signal	regfile_out	:	std_logic_vector((n_ram*2)-1 downto 0);

	-- data to be stored
	signal	data_w		:	std_logic_vector((n_ram*2)-1 downto 0) := x"0000_0000";

	-- fsm
	type state_machine is (INIT , W1 , W2 , W3 , W4 , R1 , R2 , R3 , R4 , O);
	signal	state	:	state_machine := INIT;

	-- counter of resets (num of write/reads done)
	signal	count_rst	:	std_logic_vector(15 downto 0) := (others => '0');
	signal	addr_disp	:	std_logic_vector(31 downto 0);	-- addr in to the 7seg display

	-- request logic
	signal	req_hold	:	std_logic;

begin

-----------------------------------------------
-----------------------------------------------
-- Output to 7 segment display

	test_l_sdisp <=	sel_sdisp;

	en_l_sdisp <=	'0'	when (state = O) else	-- segments enabled in '0'
					'1';

	-- reg output
	regfile_out <= regfile(to_integer(unsigned(sel_reg)));

	din_sdisp	<=	count_rst					when	(sel_rst = '1')						else	-- the display also show the numbers of rst done, and the current addr,
					addr_disp(31 downto 16)		when	(sel_addr = '1' AND sel_up = '1')	else	-- so we can verify if the test around the ram controller is also working
					addr_disp(15 downto 0)		when	(sel_addr = '1' AND sel_up = '0')	else
					x"0F01"						when	(state = W1)						else
					x"0F02"						when	(state = W2) 						else
					x"0F03"						when	(state = W3) 						else
					x"0F04"						when	(state = W4) 						else
					x"F001"						when	(state = R1) 						else
					x"F002"						when	(state = R2) 						else
					x"F003"						when	(state = R3) 						else
					x"F004"						when	(state = R4) 						else
					regfile_out(15 downto 0)	when	(sel_up = '0')						else
					regfile_out(31 downto 16);

	dout_sdisp <=	dout_sdisp_t;

	addr_disp(31 downto n_ramaddr)	<=	(others => '0');
	addr_disp(n_ramaddr-1 downto 0)	<=	addr_bus;

-----------------------------------------------
-----------------------------------------------
-- Reset Logic
	process(clk)
	begin
		if (rising_edge(clk)) then

			if (state = R4 AND ready_req = '1') then
				rst_int <= '1';		-- this way we need to press the rst button again, to restart the fsm cycle (w1 through r4)
			elsif (rst_btd = '1') then
				rst_int <= '0';
			end if;

		end if;
	end process;
			
	rst		<= rst_int OR rst_btd;
	rst_led <= rst;

	-- Reset Counter
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				----------------------------
				when O =>
					if (rst = '0') then
						count_rst <= std_logic_vector(unsigned(count_rst) + 1);
					end if;
				----------------------------
				when others =>
					null;
				----------------------------
			end CASE;
		end if;
	end process;

-----------------------------------------------
-----------------------------------------------
-- RAM Interface Signals
	stop_req <=	'1' when (state = O OR rst = '1') else
				'0';

	-- memory access request
	with state select
		request_t	<=	'1'	when	W1 | W2 | W3 | W4 | R1 | R2 | R3 | R4,
						'0'	when	others;

		request		<=	'1'	when	(request_t = '1'AND req_hold = '0') else
						'0';

		process(clk)
		begin
			if(rising_edge(clk)) then
				CASE state is
					------------------------------
					when W1 | W2 | W3 | W4 | R1 | R2 | R3 | R4 =>
					
						if (request_t = '1' AND req_hold = '0') then
								if (ack_req = '1') then
									req_hold	<= '1';
								end if;

						elsif (ready_req = '1') then
							req_hold	<= '0';
						end if;
					------------------------------
					when others =>
						req_hold <= '0';
					------------------------------
				end CASE;
			end if;
		end process;

	-- we enable
	with state select
		we_ram_t<=	'1'	when	W1 | W2 | W3 | W4,
					'0'	when	others;

		we_req	<=	'1'	when	(we_ram_t = '1' AND (NOT(state = W4 AND ready_req = '1')) AND stop_req = '0') else
					'0';

-----------------------------------------------
-----------------------------------------------
-- 8x32b Register File

	with state select
		re_ram <=	'1'	when	R1 | R2 | R3 | R4,
					'0' when	others;

	we_regfile <= ready_req AND re_ram;

	-- regfile write process
	process(clk)
	begin
		if (rising_edge(clk)) then

			if (we_regfile = '1') then
				regfile(pointer) <= data_bus;
			end if;

		end if;
	end process;
			
	-- regfile pointer process
	process(clk)
	begin
		if (rising_edge(clk)) then

			if (rst = '1' OR (we_regfile = '1' AND pointer = n_regfile-1)) then
				pointer <= 0;

			elsif (we_regfile = '1') then
				pointer <= pointer + 1;

			end if;

		end if;
	end process;

-----------------------------------------------
-----------------------------------------------
-- Addr and Data Bus

	-- address bus
	process(clk)

		constant	inc			:	integer range 0 to 15 := 2;	-- inc must be 2 to test the page mode (since the page access is sequential)
		variable	addr_hold	:	std_logic_vector(n_ramaddr-1 downto 0) := (others => '0');

	begin
		if (rising_edge(clk)) then
			CASE state is
				---------------------------------------
				when INIT =>
					null;
				---------------------------------------
				when W1 =>
					if (ready_req = '1') then
						addr_hold	:=	addr_bus;
						addr_bus	<=	std_logic_vector(unsigned(addr_bus) + inc);
					end if;
				---------------------------------------
				when W2 | W3 | R1 | R2 | R3 =>
					if (ready_req = '1') then
						addr_bus <= std_logic_vector(unsigned(addr_bus) + inc);
					end if;
				---------------------------------------
				when W4 | R4 =>
					if (ready_req = '1') then
						addr_bus <= addr_hold;
					end if;
				---------------------------------------
				when O =>
					if (rst = '0') then
						-- addr_bus	<= std_logic_vector(unsigned(addr_bus) + 63); -- verify end of 128words row/ 16words page
						-- addr_bus <= std_logic_vector(unsigned(addr_bus) + 252);	-- so we can get to the end of RAM faster
						addr_bus <= std_logic_vector(unsigned(addr_bus) + 1);
					end if;
				---------------------------------------
			end CASE;
		end if;
	end process;

	-- data bus
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				---------------------------------------
				when INIT | O | R1 | R2 | R3 | R4 =>
					null;
				---------------------------------------
				when W1 | W2 | W3 | W4 =>
					if (ready_req = '1') then
						data_w <= std_logic_vector(unsigned(data_w) + 1);
					end if;
				---------------------------------------
			end CASE;
		end if;
	end process;

	with state select
		data_bus <=	data_w			when	W1 | W2 | W3 | W4,
					(others => 'Z')	when	others;					

-----------------------------------------------
-----------------------------------------------
-- Execute 8 Writes, and then 8 Reads in to the cellram
	process(clk)
	begin
		if (rising_edge(clk)) then
			CASE state is
				---------------------------------------
				when INIT =>
					state <= O;
				---------------------------------------
				when W1 =>
					if (ready_req = '1') then
						state <= W2;
					end if;
				---------------------------------------
				when W2 =>
					if (ready_req = '1') then
						state <= W3;
					end if;
				---------------------------------------
				when W3 =>
					if (ready_req = '1') then
						state <= W4;
					end if;
				---------------------------------------
				when W4 =>
					if (ready_req = '1') then
						state <= R1;
					end if;
				---------------------------------------
				when R1 =>
					if (ready_req = '1') then
						state <= R2;
					end if;
				---------------------------------------
				when R2 =>
					if (ready_req = '1') then
						state <= R3;
					end if;
				---------------------------------------
				when R3 =>
					if (ready_req = '1') then
						state <= R4;
					end if;
				---------------------------------------
				when R4 =>
					if (ready_req = '1') then
						state	<= O;
					end if;
				---------------------------------------
				when O =>
					if (rst = '1') then
						state <= O;
					else
						state <= W1;
					end if;
				---------------------------------------
			end CASE;
		end if;
	end process;

-----------------------------------------------
-----------------------------------------------
-- Port Map

	-- cellular ram interface
	cellram_interface_i: cellram_interface
	port map
	(
		clk			=> clk,
		--------------------
		stop_req	=> stop_req,
		ready_req	=> ready_req,
		ack_req		=> ack_req,
		request		=> request,
		we_req		=> we_req,
		addr_bus	=> addr_bus,
		data_bus	=> data_bus,
		--------------------
		cre_r		=> cre_r,
		ce_r		=> ce_r,
		oe_r		=> oe_r,
		ub_r		=> ub_r,
		lb_r		=> lb_r,
		we_r		=> we_r,
		clk_r		=> clk_r,
		adv_r		=> adv_r,
		wait_r		=> wait_r,
		addr_ram	=> addr_ram,
		data_ram	=> data_ram
	);

	-- rst button debounce
	rst_debounce_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> rst_bt,
		bt_out	=> rst_btd
	);

	-- regfile select debounce
	sel_reg_0_debounce_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_reg_bt(0),
		bt_out	=> sel_reg(0)
	);

	sel_reg_1_debounce_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_reg_bt(1),
		bt_out	=> sel_reg(1)
	);

	sel_up_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_up_bt,
		bt_out	=> sel_up
	);

	test_sdisp_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_sdisp_bt,
		bt_out	=> sel_sdisp
	);

	sel_rst_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_rst_bt,
		bt_out	=> sel_rst
	);

	sel_addr_i: bt_debounce
	port map
	(
		clk		=> clk,
		bt_in	=> sel_addr_bt,
		bt_out	=> sel_addr
	);

	-- 7 segment
	nexys3_sdisp_i: nexys3_sdisp
	port map
	(
		clk		=> clk,
		en_l	=> en_l_sdisp,
		test_l	=> test_l_sdisp,
		din		=> din_sdisp,
		dout	=> dout_sdisp_t,
		dsel	=> dsel
	);

end architecture behavior;
