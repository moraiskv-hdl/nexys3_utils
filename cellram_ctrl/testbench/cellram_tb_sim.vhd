library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.cellram_tb_pkg.all;

entity cellram_tb_sim is
end entity cellram_tb_sim;

architecture behavior of cellram_tb_sim is

	------------------
	-- Signals
	constant half_period	:	time := 5 ns;

	signal 	clk		:	std_logic;
	signal 	rst_bt	:	std_logic;
	signal	rst_led	:	std_logic;

	signal	sel_addr_bt	: std_logic;
	signal	sel_rst_bt	: std_logic;
	signal	sel_up_bt	: std_logic;
	signal	sel_reg_bt	: std_logic_vector(1 downto 0);
	signal	dsel		: std_logic_vector(3 downto 0);
	signal	dout_sdisp	: std_logic_vector(7 downto 0);
	signal	sel_sdisp_bt: std_logic;

	signal cre_r	:	std_logic;
	signal ce_r		:	std_logic;
	signal oe_r		:	std_logic;
	signal ub_r		:	std_logic;
	signal lb_r		:	std_logic;
	signal we_r		:	std_logic;
	signal clk_r	:	std_logic;
	signal adv_r	:	std_logic;
	signal wait_r	:	std_logic;
	signal addr_ram	:	std_logic_vector(n_ramaddr-1 downto 0);
	signal data_ram	:	std_logic_vector(n_ram-1 downto 0);

-----------------------------------------------
begin

-----------------------------------------------
-----------------------------------------------
-- Clock and Rst
	
	-- clk
	signal_clk : process
	begin

		clk <= '0';		wait for half_period;
		loop
			clk <= '1';	wait for half_period;		-- Period = 10 ns , Freq = 100 MHz , Period/2 = 5 ns
			clk <= '0';	wait for half_period;
		end loop;

	end process signal_clk;

	-- rst
	rst_button : process	-- note: the rst_debounce must be disabled in 'bt_debounce.vhd' (stability time ~0)
	begin

		rst_bt <= '0';			wait for 170 us;	-- equal to the 'ram_interface.vhd' start up time

		loop

			rst_bt <= '1';		wait for 20 ns;
			rst_bt <= '0';		wait for 2 us;	-- enough time to end a test with page_en = '0' OR '1'
			wait for 30 ns;

		end loop;
	end process rst_button;

	-- sel buttons
	sel_addr_bt <= '0';
	sel_rst_bt	<= '0';
	sel_up_bt	<= '0';
	sel_reg_bt	<= "00";
	sel_sdisp_bt<= '0';

-----------------------------------------------
-----------------------------------------------
-- Port Map
	cellram_tb_i:	cellram_tb
	port map
	(
		clk			=> clk,
		rst_bt		=> rst_bt,
		rst_led		=> rst_led,
		-----------------------
		sel_addr_bt	=> sel_addr_bt,
		sel_rst_bt	=> sel_rst_bt,
		sel_up_bt	=> sel_up_bt,
		sel_reg_bt	=> sel_reg_bt,
		dsel		=> dsel,
		dout_sdisp	=> dout_sdisp,
		sel_sdisp_bt=> sel_sdisp_bt,
		-----------------------
		clk_r		=> clk_r,
		adv_r		=> adv_r,
		cre_r		=> cre_r,
		wait_r		=> wait_r,
		ce_r		=> ce_r,
		oe_r		=> oe_r,
		we_r		=> we_r,
		lb_r		=> lb_r,
		ub_r		=> ub_r,
		addr_ram	=> addr_ram,
		data_ram	=> data_ram
	);

	cellram_i: cellram
	port map
	(
		clk		=> clk_r,
		adv_n	=> adv_r,
		cre		=> cre_r,
		o_wait	=> wait_r,
		ce_n	=> ce_r,
		oe_n	=> oe_r,
		we_n	=> we_r,
		lb_n	=> lb_r,
		ub_n	=> ub_r,
		addr	=> addr_ram,
		dq		=> data_ram
	);

end architecture behavior;
