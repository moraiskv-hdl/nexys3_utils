library ieee;
use ieee.std_logic_1164.all;

package cellram_tb_pkg is

	constant n_ram		:	integer := 16;
	constant n_ramaddr	:	integer := 23;

	-- Cellular RAM Testbench
	COMPONENT cellram_tb is
		port
		(
			clk			: in std_logic;
			rst_bt		: in std_logic;
			rst_led		: out std_logic;
			--------------------------
			sel_addr_bt	: in std_logic;
			sel_rst_bt	: in std_logic;
			sel_up_bt	: in std_logic;
			sel_reg_bt	: in std_logic_vector(1 downto 0);
			dsel		: out std_logic_vector(3 downto 0);
			dout_sdisp	: out std_logic_vector(7 downto 0);
			sel_sdisp_bt: in std_logic;
			--------------------------
			clk_r		: out std_logic;
			adv_r		: out std_logic;
			cre_r		: out std_logic;
			wait_r		: in std_logic;
			ce_r		: out std_logic;
			oe_r		: out std_logic;
			we_r		: out std_logic;
			lb_r		: out std_logic;
			ub_r		: out std_logic;
			addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
			data_ram	: inout std_logic_vector(n_ram-1 downto 0)
		);
	end COMPONENT cellram_tb;

	-- Cellular RAM
	COMPONENT cellram is
		port
		(
			clk		: in std_logic;
			adv_n	: in std_logic;
			cre		: in std_logic;
			o_wait	: out std_logic;
			ce_n	: in std_logic;
			oe_n	: in std_logic;
			we_n	: in std_logic;
			lb_n	: in std_logic;
			ub_n	: in std_logic;
			addr	: in std_logic_vector(n_ramaddr-1 downto 0);
			dq		: inout std_logic_vector(n_ram-1 downto 0)
		);
	end COMPONENT cellram;

	-- RAM Interface
	COMPONENT cellram_interface is
		port
		(
			clk			: in std_logic;
			-------------------------
			stop_req	: in std_logic;
			we_req		: in std_logic;
			ready_req	: out std_logic;
			ack_req		: out std_logic;
			request		: in std_logic;
			addr_bus	: in std_logic_vector(n_ramaddr-1 downto 0);
			data_bus	: inout std_logic_vector((n_ram*2)-1 downto 0);
			-------------------------
			cre_r		: out std_logic;
			ce_r		: out std_logic;
			oe_r		: out std_logic;
			ub_r		: out std_logic;
			lb_r		: out std_logic;
			we_r		: out std_logic;
			clk_r		: out std_logic;
			adv_r		: out std_logic;
			wait_r		: in std_logic;
			addr_ram	: out std_logic_vector(n_ramaddr-1 downto 0);
			data_ram	: inout std_logic_vector(n_ram-1 downto 0)
		);
	end COMPONENT cellram_interface;

	-- others test components
	COMPONENT nexys3_sdisp is
		port
		(
			clk		: in std_logic;
			en_l	: in std_logic;
			test_l	: in std_logic;
			din		: in std_logic_vector(15 downto 0);
			dout	: out std_logic_vector(7 downto 0);
			dsel	: out std_logic_vector(3 downto 0)
		);
	end COMPONENT nexys3_sdisp;

	COMPONENT bt_debounce is
		port
		(
			clk		: in std_logic;
			bt_in	: in std_logic;
			bt_out	: out std_logic
		);
	end COMPONENT bt_debounce;

end package cellram_tb_pkg;
